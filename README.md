## bota_driver (noetic) - 0.6.1-2

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Thu, 07 Oct 2021 13:42:15 -0000`

These packages were released:
- `bota_driver`
- `bota_driver_testing`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.1-1`
- old version: `0.6.1-1`
- new version: `0.6.1-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.6.1-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Tue, 05 Oct 2021 13:59:32 -0000`

These packages were released:
- `bota_driver`
- `bota_driver_testing`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-3`
- old version: `0.6.0-3`
- new version: `0.6.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.6.1-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Tue, 05 Oct 2021 13:26:46 -0000`

These packages were released:
- `bota_driver`
- `bota_driver_testing`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-5`
- old version: `0.6.0-5`
- new version: `0.6.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.6.0-3

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Fri, 25 Jun 2021 08:14:53 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-2`
- old version: `0.6.0-2`
- new version: `0.6.0-3`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.6.0-5

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Fri, 25 Jun 2021 08:06:19 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-3`
- old version: `0.6.0-4`
- new version: `0.6.0-5`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.6.0-4

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver -d` on `Fri, 25 Jun 2021 08:02:24 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-3`
- old version: `0.6.0-3`
- new version: `0.6.0-4`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.6.0-2

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Thu, 24 Jun 2021 10:12:36 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-1`
- old version: `0.6.0-1`
- new version: `0.6.0-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.6.0-3

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Thu, 24 Jun 2021 09:04:18 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.6.0-2`
- old version: `0.6.0-2`
- new version: `0.6.0-3`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.6.0-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Wed, 23 Jun 2021 12:21:45 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.9-1`
- old version: `0.5.9-1`
- new version: `0.6.0-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.6.0-2

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Wed, 23 Jun 2021 12:08:57 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.9-1`
- old version: `0.6.0-1`
- new version: `0.6.0-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.6.0-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Wed, 23 Jun 2021 07:41:09 -0000`

These packages were released:
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.9-1`
- old version: `0.5.9-1`
- new version: `0.6.0-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.1`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (kinetic) - 0.5.9-2

The packages in the `bota_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic bota_driver` on `Sat, 06 Feb 2021 22:35:24 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.8-1`
- old version: `0.5.9-1`
- new version: `0.5.9-2`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (kinetic) - 0.5.9-1

The packages in the `bota_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic bota_driver` on `Sat, 06 Feb 2021 22:27:53 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.8-1`
- old version: `0.5.8-1`
- new version: `0.5.9-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.9-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Sat, 06 Feb 2021 22:12:33 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.8-1`
- old version: `0.5.8-1`
- new version: `0.5.9-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.9-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Sat, 06 Feb 2021 22:00:40 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.8-1`
- old version: `0.5.8-1`
- new version: `0.5.9-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.20.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.8-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver` on `Fri, 13 Nov 2020 09:42:46 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.7-1`
- old version: `0.5.7-1`
- new version: `0.5.8-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (kinetic) - 0.5.8-1

The packages in the `bota_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic bota_driver` on `Fri, 13 Nov 2020 09:29:16 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.6-1`
- old version: `0.5.6-1`
- new version: `0.5.8-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.8-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Fri, 13 Nov 2020 09:23:15 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.7-1`
- old version: `0.5.7-1`
- new version: `0.5.8-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.7-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver --edit` on `Wed, 04 Nov 2020 13:38:43 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.5-1`
- old version: `0.5.6-1`
- new version: `0.5.7-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.7-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Wed, 04 Nov 2020 13:25:24 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.5-1`
- old version: `0.5.6-1`
- new version: `0.5.7-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (kinetic) - 0.5.6-1

The packages in the `bota_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic bota_driver --edit` on `Tue, 03 Nov 2020 17:34:43 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.5-1`
- old version: `0.5.5-1`
- new version: `0.5.6-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.6-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Tue, 03 Nov 2020 16:45:15 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.5-1`
- old version: `0.5.5-1`
- new version: `0.5.6-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.6-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver --edit` on `Tue, 03 Nov 2020 16:28:41 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.5-1`
- old version: `0.5.5-1`
- new version: `0.5.6-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.5-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver --edit` on `Tue, 03 Nov 2020 13:17:33 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.4-2`
- old version: `0.5.4-2`
- new version: `0.5.5-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (kinetic) - 0.5.5-1

The packages in the `bota_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic bota_driver --edit` on `Tue, 03 Nov 2020 08:48:25 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.4-1`
- old version: `0.5.4-1`
- new version: `0.5.5-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.5-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Mon, 02 Nov 2020 18:03:37 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.4-1`
- old version: `0.5.4-1`
- new version: `0.5.5-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.4-2

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver --edit` on `Mon, 02 Nov 2020 15:29:22 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.5.4-1`
- new version: `0.5.4-2`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (noetic) - 0.5.4-1

The packages in the `bota_driver` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --rosdistro noetic --track noetic bota_driver --edit` on `Mon, 02 Nov 2020 14:50:59 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.5.4-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (kinetic) - 0.5.4-1

The packages in the `bota_driver` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic bota_driver --edit` on `Mon, 02 Nov 2020 10:30:37 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.5.4-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.4-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Sun, 01 Nov 2020 18:11:55 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.3-2`
- old version: `0.5.3-2`
- new version: `0.5.4-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.3-2

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Fri, 30 Oct 2020 10:39:21 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.2-2`
- old version: `0.5.3-1`
- new version: `0.5.3-2`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.3-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Fri, 30 Oct 2020 10:10:28 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.2-2`
- old version: `0.5.2-2`
- new version: `0.5.3-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.2-2

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Thu, 29 Oct 2020 10:04:29 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.1-1`
- old version: `0.5.2-1`
- new version: `0.5.2-2`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.2-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Thu, 29 Oct 2020 08:57:12 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.1-1`
- old version: `0.5.1-1`
- new version: `0.5.2-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.1-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Tue, 27 Oct 2020 11:26:22 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `bota_node`
- `bota_signal_handler`
- `bota_worker`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.0-6`
- old version: `0.5.0-6`
- new version: `0.5.1-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.0-6

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Thu, 22 Oct 2020 10:54:24 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.0-3`
- old version: `0.5.0-5`
- new version: `0.5.0-6`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.0-5

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Thu, 22 Oct 2020 10:43:35 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.0-3`
- old version: `0.5.0-4`
- new version: `0.5.0-5`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.0-4

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver` on `Thu, 22 Oct 2020 10:34:28 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: https://gitlab.com/botasys/bota_driver-release.git
- rosdistro version: `0.5.0-3`
- old version: `0.5.0-3`
- new version: `0.5.0-4`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.0-3

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Wed, 21 Oct 2020 20:49:24 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.5.0-2`
- new version: `0.5.0-3`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.0-2

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Wed, 21 Oct 2020 08:35:08 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.5.0-1`
- new version: `0.5.0-2`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## bota_driver (melodic) - 0.5.0-1

The packages in the `bota_driver` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --rosdistro melodic --track melodic bota_driver --edit` on `Wed, 21 Oct 2020 07:51:12 -0000`

These packages were released:
- `bota_device_driver`
- `bota_driver`
- `rokubimini`
- `rokubimini_bus_manager`
- `rokubimini_description`
- `rokubimini_ethercat`
- `rokubimini_examples`
- `rokubimini_factory`
- `rokubimini_manager`
- `rokubimini_msgs`
- `rokubimini_serial`

Version of package(s) in repository `bota_driver`:

- upstream repository: https://gitlab.com/botasys/bota_driver.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.5.0-1`

Versions of tools used:

- bloom version: `0.10.0`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.19.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


# bota_driver-release

The release repo of bota_driver ROS package. 